## Technologies
Project is created with:
* Java "11.0.11"
* Apache Maven 3.6.3

## Setup
To run this project with tests, install it locally using mvn:
```
mvn clean install
```

## Tests
To run only tests:
```
mvn test
```