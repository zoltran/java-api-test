package model.method.fermentation;

import model.method.fermentation.temp.Temp;

public class Fermentation {

    private Temp temp;

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }
}
