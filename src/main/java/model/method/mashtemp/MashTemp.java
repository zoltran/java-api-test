package model.method.mashtemp;

import model.method.mashtemp.temp.Temp;

public class MashTemp {

    private Temp temp;

    private Integer duration;

    public Temp getTemp() {
        return temp;
    }

    public void setTemp(Temp temp) {
        this.temp = temp;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
