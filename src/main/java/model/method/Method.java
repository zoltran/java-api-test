package model.method;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.method.fermentation.Fermentation;
import model.method.mashtemp.MashTemp;

import java.util.List;

public class Method {

    @JsonProperty("mash_temp")
    private List<MashTemp> mashTemp;

    public List<MashTemp> getMashTemp() {
        return mashTemp;
    }

    private Fermentation fermentation;

    private String twist;

    public void setMashTemp(List<MashTemp> mashTemp) {
        this.mashTemp = mashTemp;
    }

    public Fermentation getFermentation() {
        return fermentation;
    }

    public void setFermentation(Fermentation fermentation) {
        this.fermentation = fermentation;
    }

    public String getTwist() {
        return twist;
    }

    public void setTwist(String twist) {
        this.twist = twist;
    }
}