package model;

import com.fasterxml.jackson.annotation.JsonProperty;
import model.boilVolume.BoilVolume;
import model.ingredients.Ingredients;
import model.method.Method;
import model.volume.Volume;

import java.util.List;

public class Beer {

    private int id;

    private String name;

    private String tagline;

    @JsonProperty("first_brewed")
    private String firstBrewed;

    private String description;

    @JsonProperty("image_url")
    private String imageUrl;

    private double abv;

    private int ibu;

    @JsonProperty("target_fg")
    private int targetFg;

    @JsonProperty("target_og")
    private int targetOg;

    private int ebc;

    private int srm;

    private double ph;

    @JsonProperty("attenuation_level")
    private int attenuationLevel;

    @JsonProperty("boil_volume")
    private BoilVolume boilVolume;

    private Method method;

    private Volume volume;

    private Ingredients ingredients;

    @JsonProperty("food_pairing")
    private List<String> foodPairing;

    private String brewers_tips;

    @JsonProperty("contributed_by")
    private String contributedBy;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getFirstBrewed() {
        return firstBrewed;
    }

    public void setFirstBrewed(String firstBrewed) {
        this.firstBrewed = firstBrewed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public int getIbu() {
        return ibu;
    }

    public void setIbu(int ibu) {
        this.ibu = ibu;
    }

    public int getTargetFg() {
        return targetFg;
    }

    public void setTargetFg(int targetFg) {
        this.targetFg = targetFg;
    }

    public int getTargetOg() {
        return targetOg;
    }

    public void setTargetOg(int targetOg) {
        this.targetOg = targetOg;
    }

    public int getEbc() {
        return ebc;
    }

    public void setEbc(int ebc) {
        this.ebc = ebc;
    }

    public int getSrm() {
        return srm;
    }

    public void setSrm(int srm) {
        this.srm = srm;
    }

    public double getPh() {
        return ph;
    }

    public void setPh(double ph) {
        this.ph = ph;
    }

    public int getAttenuationLevel() {
        return attenuationLevel;
    }

    public void setAttenuationLevel(int attenuationLevel) {
        this.attenuationLevel = attenuationLevel;
    }

    public BoilVolume getBoilVolume() {
        return boilVolume;
    }

    public void setBoilVolume(BoilVolume boilVolume) {
        this.boilVolume = boilVolume;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public Volume getVolume() {
        return volume;
    }

    public void setVolume(Volume volume) {
        this.volume = volume;
    }

    public Ingredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(Ingredients ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getFoodPairing() {
        return foodPairing;
    }

    public void setFoodPairing(List<String> foodPairing) {
        this.foodPairing = foodPairing;
    }

    public String getBrewers_tips() {
        return brewers_tips;
    }

    public void setBrewers_tips(String brewers_tips) {
        this.brewers_tips = brewers_tips;
    }

    public String getContributedBy() {
        return contributedBy;
    }

    public void setContributedBy(String contributedBy) {
        this.contributedBy = contributedBy;
    }
}
