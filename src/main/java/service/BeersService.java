package service;

import com.netflix.config.DynamicProperty;
import model.Beer;
import model.method.mashtemp.MashTemp;
import model.method.mashtemp.temp.Temp;

import java.util.Arrays;
import java.util.Comparator;

import static io.restassured.RestAssured.given;

public class BeersService {

    private final String apiUrl;

    private final Beer[] beers;

    public BeersService() {
        this.apiUrl = DynamicProperty.getInstance("api.url").getString();
        this.beers = getMappedBeerResponse();
    }

    public Beer[] getBeersResponse() {
        return beers;
    }

    public int getHigherValueForAttenuationLevel() {
        return Arrays.stream(beers)
                .map(Beer::getAttenuationLevel)
                .max(Comparator.comparing(Integer::valueOf))
                .get();
    }

    public int getLowestMashTemp() {
        return Arrays.stream(beers)
                .map(Beer::getMethod)
                .flatMap(m -> m.getMashTemp().stream())
                .map(MashTemp::getTemp)
                .map(Temp::getValue)
                .min(Comparator.comparing(Integer::valueOf))
                .get();
    }

    private Beer[] getMappedBeerResponse() {
        return given()
                .log().uri()
                .get(apiUrl)
                .as(Beer[].class);
    }
}
