import org.testng.annotations.Test;
import service.BeersService;

import static org.fest.assertions.Assertions.assertThat;

public class ApiTests {

    @Test
    public void shouldCheckNumberOfObjectsInResponse() {
        // given
        BeersService beersService = new BeersService();

        // when
        int objectSize = beersService.getBeersResponse().length;

        // then
        assertThat(objectSize)
                .as("Wrong object number")
                .isEqualTo(25);
    }

    @Test
    public void shouldCheckHigherValueForAttenuationLevel() {
        // given
        BeersService beersService = new BeersService();

        // when
        int maxValue = beersService.getHigherValueForAttenuationLevel();

        // then
        assertThat(maxValue)
                .as("Wrong max Attenuation value")
                .isEqualTo(100);
    }

    @Test
    public void shouldCheckLowestMashTemp() {
        // given
        BeersService beersService = new BeersService();

        // when
        int minValue = beersService.getLowestMashTemp();

        // then
        assertThat(minValue)
                .as("Wrong Lowest Mash Temp")
                .isEqualTo(60);
    }
}